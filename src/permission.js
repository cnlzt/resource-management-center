import router from '@/router'
import nprogress from 'nprogress'
import 'nprogress/nprogress.css'
import store from '@/store'

// 声明报名单
const whiteList = ['/login', '/404']
// 前置守卫
router.beforeEach(async(to, from, next) => {
  nprogress.start()
  if (store.getters.token) {
    // token存在，判断to的页面是否是登录页
    if (to.path === '/login') {
      next('/')
      nprogress.done()
    } else {
      if (!store.getters.userId) {
        await store.dispatch('user/getUserInfo')
        console.log('asdf')
      }
      next()
    }
  } else {
    // token不存在判断路径是否是登录和404
    if (whiteList.includes(to.path)) {
      next()
    } else {
      //
      next('/login')
      nprogress.done()
    }
  }
})

// 后置守卫
router.afterEach(() => {
  nprogress.done()
})
