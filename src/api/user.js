import request from '@/utils/request'

export function login(data) {
  return request({
    url: 'sys/login',
    method: 'POST',
    data
  })
}

export function getUserInfo() {
  return request({
    url: '/sys/profile',
    method: 'GET'
  })
}

// 修改密码接口
export function changePassword(data) {
  return request({
    url: '/sys/user/updatePass',
    method: 'PUT',
    data
  })
}
