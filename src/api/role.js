import request from '@/utils/request'

// 获取角色列表
// 传入的数据为空时，默认为拉取第一页数据
export function getRoleList(params) {
  return request({
    url: '/sys/role',
    params // 查询参数
  })
}

// 传入角色信息添加新角色接口
export function addRole(data) {
  return request({
    url: '/sys/role',
    method: 'POST',
    data
  })
}

// 传入修改信息对角色信息进行修改
// 通过id获取角色信息
export function getRole(id) {
  return request({
    url: `/sys/role/${id}`,
    method: 'GET'
  })
}
// 修改角色信息
export function updateRole(id, data) {
  return request({
    url: `/sys/role/${id}`,
    method: 'PUT',
    data
  })
}

// 行内编辑角色信息
export function updateChange(data) {
  return request({
    url: `/sys/role/${data.id}`,
    method: 'PUT',
    data
  })
}

// 删除角色信息
export function delRole(id) {
  return request({
    url: `/sys/role/${id}`,
    method: 'DELETE'
  })
}
