import { getToken, setToken, removeToken } from '@/utils/auth'
import { getUserInfo, login } from '@/api/user'

const state = {
  token: getToken(), // 从缓存中读取token
  userInfo: {}
}
const mutations = {
  setToken(state, token) {
    state.token = token
    setToken(token) // 存入缓存
  },
  removeToken() {
    // 删除缓存中的token
    state.token = null
    removeToken()
  },
  // 用户基本数据同步保存到state中
  setUserInfo(state, data) {
    state.userInfo = data
  }
}

// actions异步获取token
const actions = {
  // context上下文，传入参数
  async login(context, data) {
    console.log(data)
    // todo: 调用登录接口
    console.log('login')
    const token = await login(data)
    // // 返回一个token 123456
    context.commit('setToken', token)
  },

  // 获取token代表登陆成功，跳转页面开始获取用户基本信息
  async getUserInfo(context) {
    const data = await getUserInfo()
    context.commit('setUserInfo', data)
    console.log(data, '???')
  },

  // 退出登录
  loginout(context) {
    context.commit('removeToken')
    context.commit('setUserInfo', {})
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
