/**
 *
 * 列表型数据转化树形
 */

export function transListToTreeData(list, rootValue) {
  const arr = []
  list.forEach(item => {
    if (item.pid === rootValue) {
      const children = transListToTreeData(list, item.id)
      arr.push(item)
      // item.children = children

      //  判断当前部门是否有子部门， 如果没有, 当前对象就没有一个为空数组的children属性
      //   级联组件也就不会显示nodata的窗口
      if (children.length) {
        item.children = children
      } // 将子节点赋值给当前节点
    }
  })

  return arr
}
