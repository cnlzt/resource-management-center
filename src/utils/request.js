import axios from 'axios'
// 引入store
import store from '@/store'
// 引入element-ui
import { Message } from 'element-ui'
import router from '@/router'

const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: 10000
})// 创建的axios对象

// http://ihrm.itheima.net/prod-api/sys/login
service.interceptors.request.use((config) => {
  // store.getters.token=>
  // console.log(store.getters)
  if (store.getters.token) {
    config.headers.Authorization = `Bearer ${store.getters.token}`
  }
  return config
}, (error) => {
  // 失败执行
  return Promise.reject(error)
})

service.interceptors.response.use((response) => {
  if (response.data instanceof Blob) return response.data
  const { data, message, success } = response.data
  if (success) {
    return data
  } else {
    Message({ type: 'warning', message })
    return Promise.reject(new Error(message))
  }
}, async(error) => {
  if (error.response?.status === 401) {
    Message({ type: 'warning', message: 'token超时' })
    await store.dispatch('user/loginout')
    router.push('/')
    return Promise.reject(error)
  }
  console.log('shibai')
  Message({ type: 'error', message: error.message })
  return Promise.reject(error)
})

export default service
